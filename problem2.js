const file ='./lipsum.txt';
const fs=require('fs')
const path=require('path')

const problem2 = fs.readFile(path.resolve(__dirname,file),"utf8",(err,data)=>{
if(err){
    console.error(err)
    } else {
        console.log("Succesfully Read a flipsum file \n")
        const dataInupperCase = data.toUpperCase()
        fs.writeFile(path.resolve(__dirname,'./upperCase.txt'),dataInupperCase,(err,data) => {
            if(err){
                console.error(err)
            } else {
                console.log("Data write in upper case form\n")
                fs.readFile(path.resolve(__dirname,'./upperCase.txt'),'utf8',(err,data) => {
                    if(err){
                        console.error(err)
                    } else{
                        console.log("File Read from UpperCase file\n")
                        // console.log(data)
                        const fromUpperToLowerCaseAndSplit = data.toLowerCase().split('. ').join('\n')
                        // console.log(fromUpperToLowerCaseAndSplit)
                        fs.writeFile(path.resolve(__dirname,'./lowerCase_WithSplit.txt'),fromUpperToLowerCaseAndSplit,(err, data)=> {
                            if(err){
                                console.error(err)
                            } else {
                                console.log("Succesfully Write Lower file in Uppercase_withSplit\n")
                                fs.appendFile(path.resolve(__dirname,"./filenames.txt" ) ,'./upperCase.txt  ' , (err) => {
                                    if(err){
                                        console.error(err)
                                    } else {
                                        console.log("successfully Added 1- files in filenames\n")
                                        fs.appendFile(path.resolve(__dirname,"./filenames.txt") , 'lowerCase_WithSplit.txt  ' ,(err) => {
                                            if(err){
                                                console.error(err)
                                            } else {
                                                console.log("Successfully Added 2 - file in filenames\n")
                                                const forSort = fromUpperToLowerCaseAndSplit.split('\n').sort().join('\n')
                                                // console.log(forSort)
                                                fs.writeFile(path.resolve(__dirname,'./sortedfile.txt'),forSort,(err,data) => {
                                                    if(err){
                                            
                                                        console.error(err)
                                                    } else {
                                                        console.log("Sorted file\n")
                                                        fs.appendFile(path.resolve(__dirname,'./filenames.txt'), './sortedfile.txt  ',
                                                        (err) => {
                                                            if(err){
                                                                console.error(err)
                                                            } else {
                                                                console.log("Successfully added 3rd file which is sorted data\n")
                                                                fs.readFile(path.resolve(__dirname,'./filenames.txt'),'utf8',(err,data) => {
                                                                    if(err){
                                                                        console.error(err)
                                                                    } else {
                                                                        const arrayOfFilenmaes =data.split(' ')
                                                                        arrayOfFilenmaes.map(x => {
                                                                            fs.unlink(path.resolve(__dirname,x), (err) => {
                                                                                if(err){
                                                                                    console.error(err)
                                                                                } else {
                                                                                    console.log("successfully deleted all the created files......")
                                                                                }
                                                                            })
                                                                            
                                                                        })


                                                                    }
                                                                
                                                                }) 

                                                            }
                                        
                                                        })
                                                    }
                                                })
                                                
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
})

module.exports = problem2